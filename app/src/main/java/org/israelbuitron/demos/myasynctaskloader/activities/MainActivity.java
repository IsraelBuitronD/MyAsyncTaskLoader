package org.israelbuitron.demos.myasynctaskloader.activities;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import org.israelbuitron.demos.myasynctaskloader.R;
import org.israelbuitron.demos.myasynctaskloader.adapters.PeopleAdapter;
import org.israelbuitron.demos.myasynctaskloader.beans.Person;
import org.israelbuitron.demos.myasynctaskloader.loaders.MyAsyncTaskLoader;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<List<Person>> {

    private PeopleAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdapter = new PeopleAdapter(this, new ArrayList<Person>());

        ListView employeeListView = (ListView) findViewById(R.id.people);
        employeeListView.setAdapter(mAdapter);

        getSupportLoaderManager().initLoader(1, null, this).forceLoad();
    }
    @Override
    public Loader<List<Person>> onCreateLoader(int id, Bundle args) {
        return new MyAsyncTaskLoader(MainActivity.this);
    }
    @Override
    public void onLoadFinished(Loader<List<Person>> loader, List<Person> data) {
        mAdapter.setPeople(data);
    }
    @Override
    public void onLoaderReset(Loader<List<Person>> loader) {
        mAdapter.setPeople(new ArrayList<Person>());
    }

}
