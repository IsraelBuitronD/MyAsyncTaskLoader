package org.israelbuitron.demos.myasynctaskloader.beans;

/**
 * @author Israel Buitrón
 */
public class Person {

    private String mId;

    private String mName;

    public Person(String id, String name) {
        mId = id;
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }
}
