package org.israelbuitron.demos.myasynctaskloader.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import org.israelbuitron.demos.myasynctaskloader.beans.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * @author israel.buitron
 */
public class MyAsyncTaskLoader extends AsyncTaskLoader<List<Person>> {

    public MyAsyncTaskLoader(Context context) {
        super(context);
    }

    @Override
    public List<Person> loadInBackground() {
        List<Person> list = new ArrayList<Person>();

        list.add(new Person("1", "Octavio Paz Lozano"));
        list.add(new Person("2", "Alfonso García Robles"));
        list.add(new Person("3", "Mario Molina Pasquel y Henríquez"));

        return list;
    }
}
